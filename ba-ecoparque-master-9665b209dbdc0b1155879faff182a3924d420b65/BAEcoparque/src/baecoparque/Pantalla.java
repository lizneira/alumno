
package baecoparque;

import javax.swing.JOptionPane;
        
public class Pantalla {
    
    public void mostrarMensaje(String mensaje) {
        System.out.print(mensaje);
    }

    public void mostrarLineaMensaje(String mensaje) {
        System.out.println(mensaje);
    }
    
    public boolean leerBoolean(String texto){
        int i = JOptionPane.showConfirmDialog(null, texto, "Consulta",JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }
}
