package baecoparque;

public class MenuPrincipal {

    private int opcion;
    private boolean usuarioAutenticado; // indica si el usuario es autenticado
    private Pantalla pantalla;
    private Teclado teclado; // teclado del BAE
    private BaseDatos baseDatos;

    public MenuPrincipal() {
        usuarioAutenticado = false;
        pantalla = new Pantalla();
        teclado = new Teclado(); // crea el teclado
        baseDatos= new BaseDatos();
    }

    public void menuPrincipal() {
        pantalla.mostrarLineaMensaje("..........BIENVENIDO BA Ecoparque!!..........");
        pantalla.mostrarLineaMensaje("\tElija una opcion del menu: \n");
        pantalla.mostrarLineaMensaje("1. Administrador\n");
        pantalla.mostrarLineaMensaje("2. Cuidador\n");
        pantalla.mostrarLineaMensaje("3. Guia\n");
        pantalla.mostrarLineaMensaje("4. Salir\n\n");
    }

    public void run() {
        // da la bienvenida al usuario y lo autentica; realiza transacciones
        while (true) {
            // itera mientras el usuario no haya sido autenticado

            while (!usuarioAutenticado) {
                menuPrincipal();
                opcion = teclado.obtenerEntrada();

                while (opcion < 1 || opcion > 4) {
                    pantalla.mostrarLineaMensaje("Ingrese nuevamente una opcion: ");
                    opcion = teclado.obtenerEntrada();
                } // fin de while

                if (opcion == 4) {
                    System.exit(0);
                }
                else{
                    autenticarUsuario(); // autentica el usuario 
                }
                   
//                    usuarioAutenticado = false; // restablece antes de la siguiente sesión con BAE
                pantalla.mostrarLineaMensaje("\nGracias! Adios!");
            } // fin de while
        }
    }// fin del método run

    
    
    
    // trata de autenticar al usuario en la base de datos
    private void autenticarUsuario() {
        
        pantalla.mostrarLineaMensaje("\nEscriba su numero de legajo: ");
        int numeroLegajo = teclado.obtenerEntrada();


        // establece usuarioAutenticado con el valor booleano devuelto por la base de datos
        usuarioAutenticado = baseDatos.autenticarUsuario(opcion, numeroLegajo);

//        // verifica si la autenticación tuvo éxito
//        if (usuarioAutenticado) {
//            numeroCuentaActual = numeroCuenta; // guarda el # de cuenta del usuario
//        } // fin de if
//        else {
//            pantalla.mostrarLineaMensaje("Numero de cuenta o NIP invalido. Intente de nuevo.");
//        }
    } // fin del método autenticarUsuario

}
